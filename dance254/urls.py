from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'joins.views.home', name='home'),
    # url(r'^testhome/$', 'dance254.views.testhome', name='testhome'),
    url(r'^(?P<ref_id>.*)$', 'joins.views.share', name='share'),
    # Examples:
    # url(r'^home2/$', 'dance254.views.home', name='home2'),
    # url(r'^blog/', include('blog.urls')),   
)
