from .base import * # NOQA

try:
	from .local import * # NOQA
	live = False
except:
	live = True

if live:
	from .production import * # NOQA