from django import forms

from .models import Join

# Regular Django Form


class EmailForm(forms.Form):
	name = forms.CharField(required=False)
	email = forms.EmailField()


# Model Form
class JoinForm(forms.ModelForm):
	class Meta:
		model = Join
		fields = ["email", ] # shows the fields that are displayed on the form.